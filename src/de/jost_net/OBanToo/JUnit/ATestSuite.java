/*
 * $Source: /cvsroot/obantoo/obantoo/src/de/jost_net/OBanToo/JUnit/ATestSuite.java,v $
 * $Revision: 1.3 $
 * $Date: 2014/06/09 07:24:25 $
 *
 * Copyright 2013 by Heiner Jostkleigrewe
 * Diese Datei steht unter LGPL - siehe beigefügte lpgl.txt
 */
package de.jost_net.OBanToo.JUnit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ TestIBAN.class, TestPruefziffer.class,
    TestSEPALand.class, TestUeberweisung.class })
public class ATestSuite
{
  // Nothing to do
}
